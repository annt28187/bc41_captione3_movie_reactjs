import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Loading from './components/Loading/Loading';
import Trailer from './components/Trailer/Trailer';
import { userRoutes } from './routes/userRoutes';

function App() {
  return (
    <div>
      <Loading />
      <Trailer />
      <BrowserRouter>
        <Routes>
          {userRoutes.map(({ url, component }, id) => {
            return <Route path={url} element={component} key={id} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
