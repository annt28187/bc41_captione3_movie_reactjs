import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Loading from './components/Loading/Loading';
import Trailer from './components/Trailer/Trailer';
import { userRoutes } from './routes/userRoutes';

function App() {
  return (
    <BrowserRouter>
      <Loading />
      <Trailer />
      <Routes>
        {userRoutes.map(({ url, component }) => {
          return <Route path={url} element={component} />;
        })}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
