import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { localUserServ } from '../../services/localService';
import { AccountCircle } from '@mui/icons-material';

export default function UserMenu() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let renderContent = () => {
    if (userInfo) {
      return (
        <div className="flex flex-col lg:text-xl lg:flex-row items-start lg:items-center space-y-2 lg:space-y-0 lg:space-x-2 lg:pr-3 divide-x">
          <NavLink to="/user-info">
            <button className="hover:text-[red] lg:border-2  lg:pr-2 flex items-center p-4 text-[gray]">
              <AccountCircle />
              <span className="ml-1 font-medium">{userInfo.hoTen}</span>
            </button>
          </NavLink>
          <div>
            <button
              onClick={handleLogout}
              className="hover:text-[red] lg:border-2  lg:pr-2 flex items-center text-[gray]"
            >
              <AccountCircle />
              <span className="ml-1 font-medium">Đăng xuất</span>
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="flex flex-col lg:text-xl lg:flex-row items-start lg:items-center space-y-2 lg:space-y-0 lg:space-x-2 lg:pr-3">
          <NavLink to="/sign-in">
            <button className="hover:text-[red] lg:border-2  lg:pr-2 flex items-center text-[gray]">
              <AccountCircle />
              <span className="ml-1 font-medium">Đăng nhập</span>
            </button>
          </NavLink>

          <NavLink to="/sign-up">
            <button className="hover:text-[#ff4a2e] flex items-center text-[gray]">
              <AccountCircle />
              <span className="ml-1 font-medium">Đăng ký</span>
            </button>
          </NavLink>
        </div>
      );
    }
  };
  let handleLogout = () => {
    localUserServ.remove();
    window.location.href = '/sign-in';
  };
  return <div>{renderContent()}</div>;
}
