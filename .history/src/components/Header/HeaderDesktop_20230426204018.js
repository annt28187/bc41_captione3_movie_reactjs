import React from 'react';
import UserMenu from './UserMenu';
import Logo from '../../assets/images/logo-moive.png';

export default function HeaderDesktop() {
  return (
    <div className="menu">
      <div className="flex justify-between px-4 items-center shadow-lg bg-white bg-opacity-90 ">
        <a className="flex items-center" href="/">
          <img src={Logo} className="w-20 " alt="logo" />
        </a>
        <div>
          <ul className="flex items-center text-xl font-bold">
            <li className="mr-6">
              <a className="text-[red] hover:font-medium duration-200" href="#listMovie">
                Lịch chiếu
              </a>
            </li>
            <li className="mr-6">
              <a className="text-[red] hover:font-medium duration-200" href="#tabMovie">
                Cụm rạp
              </a>
            </li>
            <li className="mr-6">
              <a className="text-[red] hover:font-medium duration-200" href="#tinTuc">
                Tin tức
              </a>
            </li>
            <li className="mr-6">
              <a className="text-[red] hover:font-medium duration-200" href="#ungDung">
                Ứng dụng
              </a>
            </li>
          </ul>
        </div>
        <div>
          <UserMenu />
        </div>
      </div>
    </div>
  );
}
