import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { message } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { SET_MOVIE_ARR } from '../../redux/constants/movieConstant';
import { movieServ } from './../../services/movieService';

export default function SearchMovieDesktop() {
  let movieArr = useSelector((state) => state.movieReducer.movieArr);
  let dispatch = useDispatch();
  let [heThongRapChieuArr, setHeThongRapChieu] = useState([]);
  let [lichChieuPhim, setLichChieuPhim] = useState([]);
  let [maPhimCu, setMaPhimCu] = useState('');
  let [maRapCu, setMaRapCu] = useState('');
  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        dispatch({
          type: SET_MOVIE_ARR,
          payload: res.data.content,
        });
      })
      .catch((err) => console.log(err));
  }, []);

  let renderRap = () => {
    return heThongRapChieuArr.map((item) => {
      return item.cumRapChieu.map((item) => {
        return <option value={item.maCumRap}>{item.tenCumRap}</option>;
      });
    });
  };

  let renderNgayGio = () => {
    return lichChieuPhim.map((item) => {
      return <option>{moment(item.ngayChieuGioChieu).format('DD-MM-YYYY ~ hh:mm')}</option>;
    });
  };

  let handleChonPhim = () => {
    let maPhim = document.getElementById('chon-phim').value;
    if (maPhim == 0) {
      document.getElementById('chon-rap').value = 0;
      setLichChieuPhim([]);
      setHeThongRapChieu([]);
      setMaPhimCu(maPhim);
    } else {
      movieServ.getDesktopSearchMovie(maPhim).then((res) => {
        setHeThongRapChieu(res.data.content.heThongRapChieu);
      });
    }
    if (maPhim != maPhimCu) {
      document.getElementById('chon-rap').value = 0;
      setLichChieuPhim([]);
      setMaPhimCu(maPhim);
    }
  };

  let handleChonRap = () => {
    let maCumRap = document.getElementById('chon-rap').value;
    if (maCumRap == 0) {
      setLichChieuPhim([]);
    }
    if (maCumRap != maRapCu) {
      heThongRapChieuArr.map((item) => {
        item.cumRapChieu.map((item) => {
          if (item.maCumRap == maCumRap) {
            setLichChieuPhim(item.lichChieuPhim);
          }
        });
      });
      document.getElementById('chon-gio').value = 0;
      setMaRapCu(maCumRap);
    }
  };

  let handleMuaVe = () => {
    let chonPhim = document.getElementById('chon-phim').value;
    let chonRap = document.getElementById('chon-rap').value;
    let chonGio = document.getElementById('chon-gio').value;
    if (chonPhim == 0) {
      message.warning('Vui lòng chọn phim', 1);
    } else if (chonRap == 0) {
      message.warning('Vui lòng chọn rạp', 1);
    } else if (chonGio == 0) {
      message.warning('Vui lòng chọn ngày giờ chiếu', 1);
    } else {
      window.location.href = '*';
    }
  };
  return (
    <div className=" lg:px-48 my-10">
      <div className="flex space-x-4 p-[20px] rounded items-center shadow-xl " id="search">
        {/* search phim */}
        <div className="grow">
          <div className="flex items-center justify-around">
            <select
              id="chon-phim"
              className="block py-2.5 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer"
              onClick={() => {
                handleChonPhim();
              }}
              defaultValue={0}
            >
              <option value={0}>Chọn Phim </option>
              {movieArr.map((movie, index) => {
                return (
                  <option value={movie.maPhim} key={index}>
                    {' '}
                    {movie.tenPhim}
                  </option>
                );
              })}
            </select>
            <div className="pt-2">
              <ion-icon name="chevron-down-outline" size="small"></ion-icon>
            </div>
          </div>
        </div>
        {/* search rap */}
        <div className="grow">
          <div className="flex items-center justify-around ">
            <select
              id="chon-rap"
              defaultValue={0}
              onClick={() => {
                handleChonRap();
              }}
              className="block py-2.5 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer"
            >
              <option value={0}>Chọn Rạp </option>
              {renderRap()}
            </select>
            <div className="pt-2">
              <ion-icon name="chevron-down-outline" size="small"></ion-icon>
            </div>
          </div>
        </div>
        {/* search ngay gio chieu */}
        <div className="grow">
          <div className="flex items-center justify-around">
            <select
              id="chon-gio"
              defaultValue={0}
              className="block py-2.5 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer"
            >
              <option value={0}>Chọn Ngày Giờ Chiếu </option>
              {renderNgayGio()}
            </select>
            <div className="pt-2">
              <ion-icon name="chevron-down-outline" size="small"></ion-icon>
            </div>
          </div>
        </div>
        {/* button mua vé */}
        <div className="w-[200px]">
          <button
            className="bg-[#fb4226] hover:bg-[#f85941] text-white font-bold py-2 px-4 rounded-full w-full"
            onClick={() => {
              handleMuaVe();
            }}
          >
            Mua vé
          </button>
        </div>
      </div>
    </div>
  );
}
