export const dataDoiTac = [
  {
    name: 'cgv',
    logo: './images/cgv.png',
    url: 'https://www.cgv.vn/',
  },
  {
    name: 'bhd',
    logo: './images/bhdcine.png',
    url: 'https://www.bhdstar.vn/',
  },
  {
    name: 'galaxy',
    logo: './images/galaxycine.png',
    url: 'https://www.galaxycine.vn/',
  },
  {
    name: 'cinestar',
    logo: './images/cinestar.png',
    url: 'https://www.galaxycine.vn/',
  },
  {
    name: 'lotte',
    logo: './images/lottecine.png',
    url: 'https://lottecinemavn.com/LCHS/index.aspx',
  },
  {
    name: 'megaGS',
    logo: './images/megags.png',
    url: 'https://www.megagscinemas.vn/',
  },
  {
    name: 'beta',
    logo: './images/beta.jfif',
    url: 'https://www.betacinemas.vn/home.htm',
  },
  {
    name: 'ddc',
    logo: './images/ddc.png',
    url: 'http://ddcinema.vn/',
  },
  {
    name: 'touchCine',
    logo: './images/touchcine.png',
    url: 'https://touchcinema.com/',
  },
  {
    name: 'cinemax',
    logo: './images/cinemax.jfif',
    url: 'https://cinemaxvn.com/',
  },
  {
    name: 'starLight',
    logo: './images/starlight.png',
    url: 'https://starlight.vn/',
  },
  {
    name: 'dCine',
    logo: './images/dcine.png',
    url: 'https://www.dcine.vn/',
  },
  {
    name: 'zaloPay',
    logo: './images/zaloPay.png',
    url: 'https://zalopay.vn/',
  },
  {
    name: 'payoo',
    logo: './images/payoo.png',
    url: 'https://www.payoo.vn/',
  },
  {
    name: 'vietcomBank',
    logo: './images/vietcombank.png',
    url: 'https://portal.vietcombank.com.vn/Pages/Home.aspx',
  },
  {
    name: 'agriBank',
    logo: './images/agribank.png',
    url: 'https://www.payoo.vn/',
  },
  {
    name: 'viettinBank',
    logo: './images/viettinbank.png',
    url: 'https://www.vietinbank.vn/web/home/vn/index.html',
  },
  {
    name: 'indovinabank',
    logo: './images/indovinabank.png',
    url: 'https://www.indovinabank.com.vn/',
  },
  {
    name: '123go',
    logo: './images/123go.png',
    url: 'https://webv3.123go.vn/',
  },
  {
    name: 'laban',
    logo: './images/laban.jfif',
    url: 'https://laban.vn/',
  },
];
