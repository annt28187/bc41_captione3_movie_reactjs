import React from 'react';
import { dataDoiTac } from './dataDoiTac';

export default function FooterDesktop() {
  let renderDoiTac = () => {
    return dataDoiTac.map((item, index) => {
      return (
        <div key={index} className="rounded-full overflow-x-auto w-fit">
          <a href={item.url} target="_blank" rel="noreferrer">
            <img className="w-8 object-contain" src={item.logo} alt="" />
          </a>
        </div>
      );
    });
  };

  return (
    <div id="footer" className="footerLargeDesktop">
      <div className="upperFooter container pt-3 pb-7 grid grid-cols-3">
        <div className="TIX">
          <h5>TIX</h5>
          <div className="grid grid-cols-2 gap-y-2 text-xs">
            <h6>FAQ</h6>
            <h6>Thỏa thuận sử dụng</h6>
            <h6>Brand Guidelines</h6>
            <h6>Chính sách bảo mật</h6>
          </div>
        </div>
        <div className="doiTac">
          <h5>ĐỐI TÁC</h5>
          <div className="grid grid-cols-4 gap-x-2 gap-y-5">{renderDoiTac()}</div>
        </div>
        <div className="grid grid-cols-2">
          <div className="mobileApp">
            <h5>MOBILE APP</h5>
            <div className="flex space-x-3">
              <a
                target="_blank"
                href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197"
                rel="noreferrer"
              >
                <img className="h-8 object-contain" src="./images/icon_ios.png" alt="" />
              </a>
              <a
                target="_blank"
                href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123"
                rel="noreferrer"
              >
                <img className="h-8 object-contain" src="./images/icon_android.png" alt="" />
              </a>
            </div>
          </div>
          <div className="social">
            <h5>SOCIAL</h5>
            <div className="flex space-x-3">
              <a
                target="_blank"
                href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197"
                rel="noreferrer"
              >
                <img className="w-8 object-contain" src="./images/icon_fb.png" alt="" />
              </a>
              <a
                target="_blank"
                href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123"
                rel="noreferrer"
              >
                <img className="w-8 object-contain" src="./images/icon_zalo.png" alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="lowerFooter container py-7 grid grid-cols-12">
        <div className="col-span-2">
          <img className="w-32 object-contain" src="./images/download.jfif" alt="" />
        </div>
        <div className="text-white text-xs leading-5 col-span-8">
          <h6 className="mb-2">TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</h6>
          <p>
            Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam.{' '}
            <br />
            Giấy chứng nhận đăng ký kinh doanh số: 0101659783, <br />
            đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành
            phố Hồ Chí Minh cấp. <br />
            Số Điện Thoại (Hotline): 1900 545 436
          </p>
        </div>
        <div className="col-span-2">
          <img className="w-32 object-contain" src="./images/daThongBao-logo.png" alt="" />
        </div>
      </div>
    </div>
  );
}
