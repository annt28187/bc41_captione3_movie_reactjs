import React, { useEffect, useState } from 'react';
import { Desktop, Mobile, Tablet } from '../../layouts/Responsive';
import FooterDesktop from './FooterDesktop';
import FooterTablet from './FooterTablet';
import FooterMobile from './FooterMobile';
import { movieServ } from '../../services/movieService';

export default function Footer() {
  const [theaters, setTheaters] = useState([]);
  useEffect(() => {
    movieServ
      .getListTheater()
      .then((res) => {
        setTheaters(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const renderPartners = () => {
    return theaters.map((theater, index) => {
      return (
        <div key={theater.maHeThongRap + index.toString()} className="w-1/12 m-2">
          <img
            className="w-full h-full"
            key={theater.maHeThongRap}
            src={theater.logo}
            alt="theater logo"
          />
        </div>
      );
    });
  };
  return (
    <div className="bg-gray-800 py-5">
      <div className="max-w-5xl mx-auto px-5 text-white flex flex-wrap border-b-2 border-b-gray-400 h-full">
        <div className="w-1/3 pr-1">
          <h1 className="text-md font-semibold text-white">TIX</h1>
          <ul className="flex flex-wrap justify-between">
            <li className="md:w-1/2">
              <a
                className="text-gray-400 hover:text-white transition duration-300"
                href="#"
                target="_blank"
              >
                FAQ
              </a>
            </li>
            <li className="md:w-1/2">
              <a className="text-gray-400 hover:text-white transition duration-300" href="#">
                Terms
              </a>
            </li>
            <li className="md:w-1/2">
              <a className="text-gray-400 hover:text-white transition duration-300" href="#">
                Brand Guidelines
              </a>
            </li>
            <li className="md:w-1/2">
              <a className="text-gray-400 hover:text-white transition duration-300" href="#">
                Privacy Policy
              </a>
            </li>
          </ul>
        </div>
        <div className="w-1/3">
          <h1 className="text-md font-semibold text-white">PARTNERS</h1>
          <div className="flex flex-wrap gap-2">{renderPartners()}</div>
        </div>
        <div className="w-1/3 flex flex-wrap justify-between">
          <div className="w-1/2">
            <h1 className="text-md font-semibold text-white">MOBILE APP</h1>
            <div className="flex flex-wrap">
              <a href="#">
                <i className="fa-brands fa-apple text-3xl mr-3 text-gray-500 hover:text-white transition duration-300"></i>
              </a>
              <a href="#">
                <i className="fa-brands fa-android text-3xl text-gray-500 hover:text-white transition duration-300"></i>
              </a>
            </div>
          </div>
          <div className="w-1/2">
            <h1 className="text-md font-semibold text-white">SOCIAL</h1>
            <div className="flex flex-wrap">
              <a href="#">
                <i className="fa-brands fa-facebook text-3xl mr-3 text-gray-500 hover:text-white transition duration-300"></i>
              </a>
              <a href="#">
                <i className="fa-brands fa-instagram text-3xl text-gray-500 hover:text-white transition duration-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="max-w-5xl mx-auto px-5 flex flex-wrap py-5">
        <div className="md:w-1/6 mx-auto">
          <a href="/">
            <h2 className="text-red-600 text-2xl animate-bounce font-bold tracking-widest">
              <i className="fa-solid fa-dice-four"> Kube</i>
            </h2>
          </a>
        </div>
        <div className="text-white md:w-4/6">
          <h1 className="text-white">KUBE - A PRODUCT OF KAN SHIRO CORPORATE</h1>
          <p>
            Address: 8/15A ĐT 4-2, Dong Thanh Commune, Hoc Mon District, Ho Chi Minh City, Vietnam.
            Business Registration Certificate No: 0101659783, 30th change registration, January 22,
            2020 issued by the Department of Planning and Investment of Ho Chi Minh City. Telephone
            Number (Hotline): 1900 545 436
          </p>
        </div>
        <div className="md:w-1/6 mx-auto">
          <img
            className="w-1/3 md:w-2/3 mx-auto"
            src="http://demo1.cybersoft.edu.vn/static/media/daThongBao-logo.cb85045e.png"
            alt="confirm image"
          />
        </div>
      </div>
    </div>
  );
}
