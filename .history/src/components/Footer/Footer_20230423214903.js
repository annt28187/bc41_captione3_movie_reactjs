import React from 'react';
import { Desktop } from '../../layouts/Responsive';
import FooterDesktop from './FooterDesktop';

export default function Footer() {
  return (
    <div>
      <Desktop>
        <FooterDesktop />
      </Desktop>
    </div>
  );
}
