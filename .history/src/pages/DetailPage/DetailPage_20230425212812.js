import React, { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { Progress, Rate, Tabs } from 'antd';
import { movieServ } from './../../services/movieService';
import moment from 'moment/moment';

const onChange = (key) => {};

export default function DetailPage() {
  let params = useParams();
  let [movie, setMovie] = useState({});
  const [theaterMovie, setTheaterMovie] = useState([]);
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let resultMovie = await movieServ.getDetailedMovie(params.id);
        let resultTheater = await movieServ.getTheaterDetailedMovie(params.id);
        setMovie(resultMovie.data.content);
        setTheaterMovie(resultTheater.data.content.heThongRapChieu);
      } catch (error) {}
    };
    fetchDetail();
  }, []);
  let renderLichChieuPhim = () => {
    return theaterMovie.map((heThongRap, index) => {
      return {
        key: heThongRap.maHeThongRap,
        label: (
          <div className="logoTheater">
            <img src={heThongRap.logo} alt="" className="w-10 object-contain" />
          </div>
        ),
        children: heThongRap.cumRapChieu.map((cumRap, index) => {
          return (
            <div key={index}>
              <h2 className="text-lg text-green-500">{cumRap.tenCumRap}</h2>
              <div className="grid grid-cols-4 gap-x-5">
                {cumRap.lichChieuPhim.map((date) => {
                  return (
                    <a
                      style={{ textDecoration: 'none' }}
                      className="showtime hover:font-bold border p-2 rounded"
                      href="#"
                      key={date.maLichChieu}
                    >
                      <span className="text-green-500">
                        {moment(date.ngayChieuGioChieu).format('DD-mm-yyyy ')}
                      </span>
                      ~
                      <span className="text-red-500">
                        {' '}
                        {moment(date.ngayChieuGioChieu).format('hh:mm')}
                      </span>
                    </a>
                  );
                })}
              </div>
            </div>
          );
        }),
      };
    });
  };
  return (
    <div className="mx-auto mb-20 lg:w-3/5 md:w-11/12 sm:w-full w-full flex flex-wrap justify-between py-40">
      <div className="title__movie flex">
        <div className="w-1/3 h-64 mr-5 rounded overflow-hidden">
          <img className="w-full h-full" src={movie.hinhAnh} alt="detail_image" />
        </div>
        <div className="lg:w-2/3 space-y-5">
          <h2 className="text-2xl text-red-500">{movie.tenPhim}</h2>
          <span className="block">{moment(movie.ngayKhoiChieu).format('L')}</span>
        </div>
      </div>
      <div className="rating__movie">
        <Progress
          size={150}
          type="circle"
          format={(percent) => `${percent / 10} Đ`}
          percent={movie.danhGia * 100}
          strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }}
        />
        <br />
        <Rate disabled defaultValue={5} />
      </div>
      <div className="tab__movie flex-grow-0 basis-full py-10">
        <Tabs
          style={{ width: 760 }}
          tabPosition="left"
          items={renderLichChieuPhim()}
          defaultActiveKey="1"
          onChange={onChange}
        />
      </div>
    </div>
  );
}
