import React, { useEffect } from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { useDispatch } from 'react-redux';
import { NavLink, useNavigate } from 'react-router-dom';
import { localUserServ } from '../../services/localService';
import { userServ } from './../../services/userService';
import { SET_USER_LOGIN } from '../../redux/constants/userConstant';
import { AccountCircle } from '@mui/icons-material';

export default function SignInPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  useEffect(() => {
    if (localUserServ.get() === null) return;
    setTimeout(() => {
      navigate('/');
    }, 2000);
  }, []);

  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        localUserServ.set(res.data.content);
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        message.success('Đăng nhập thành công');
        setTimeout(() => {
          navigate('/');
        }, 2000);
      })
      .catch((err) => {
        message.error('Đăng nhập thất bại');
      });
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <div className="loginPage text-2xl bg-white rounded-xl p-5 sm:w-2/3 md:w-2/5 lg:w-1/3">
      <div className="loginTitle text-center w-full space-y-5 mb-10">
        <i
          style={{ lineHeight: '45px', borderRadius: '50%' }}
          className="fas fa-user fa-lg fa-fw bg-orange-500 w-12 h-12 text-white text-2xl"
        ></i>
        <h2 className="text-3xl font-bold text-red-700 ">Đăng nhập</h2>
      </div>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 24,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical"
        className="space-y-8"
      >
        <Form.Item
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: 'Đây là trường bắt buộc!',
            },
          ]}
        >
          <Input
            className="py-3 text-gray-800 hover:border-orange-600 focus:border-orange-600"
            placeholder="Tài Khoản *"
          />
        </Form.Item>

        <Form.Item
          name="matKhau"
          rules={[
            {
              required: true,
              message: 'Đây là trường bắt buộc!',
            },
          ]}
        >
          <Input.Password
            className="py-3 text-gray-800 hover:border-orange-600 focus:border-orange-600"
            placeholder="Mật Khẩu *"
          />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Checkbox>Nhớ tài khoản</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Button
            type="primary"
            htmlType="submit"
            className="loginButton bg-orange-600 text-white mb-3 py-6 block w-full"
          >
            Đăng nhập
          </Button>
          <NavLink
            to={'/sign-up'}
            className="text-right block hover:underline underline-offset-2 decoration-black hover:decoration-black"
          >
            <span className="text-black font-medium">Bạn chưa có tài khoản? Đăng ký</span>
          </NavLink>
        </Form.Item>
      </Form>
    </div>
  );
}
