import React, { useEffect } from 'react';
import { Button, Form, Input, message } from 'antd';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { localUserServ } from '../../services/localService';
import { setUserInfor } from '../../redux/slice/userSlice';
import { userServ } from './../../services/userService';

export default function SignInPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  useEffect(() => {
    if (localUserServ.get() === null) return;
    setTimeout(() => {
      navigate('/');
    }, 2000);
  }, []);

  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        localUserServ.set(res.data.content);
        dispatch(setUserInfor(res.data.content));
        message.success('Đăng nhập thành công');
        setTimeout(() => {
          navigate('/');
        }, 2000);
      })
      .catch((err) => {
        message.error('Đăng nhập thất bại');
      });
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <div className="login-wrap">
      <div className="mx-auto h-screen w-screen flex flex-wrap justify-center items-center">
        <div className="flex-1 h-full flex items-center justify-center mx-auto px-10 md:px-20 lg:px-40 xl:px-52 2xl:px-64">
          <Form
            className="w-full pt-12 rounded-lg bg-gray-700 bg-opacity-60 backdrop-blur-sm px-5 md:px-8 lg:px-10"
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="on"
          >
            <div className="flex">
              <div className="flex-1">
                <p className="text-left text-gray-300 text-base font-bold">Xin chào</p>
                <h1 className="text-4xl font-extrabold dark:text-white text-left text-white">
                  Log in to{' '}
                  <a href="/" className="text-5xl font-black text-red-600">
                    KUBE.
                  </a>
                </h1>
              </div>
              <div className="flex-1 md:hidden flex h-full justify-center items-center"></div>
            </div>
            <Form.Item
              className="text-green-500"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: 'Please confirm your username!',
                },
              ]}
            >
              <Input
                className="input-user text-md bg text-gray-600 px-3 py-2 rounded-lg border-transparent hover:border-green-400"
                placeholder="Username *"
              />
            </Form.Item>

            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
              ]}
            >
              <Input.Password
                className="input-user text-md bg text-gray-600 px-3 py-2 rounded-lg border-transparent hover:border-green-400"
                placeholder="Password *"
              />
            </Form.Item>

            <Form.Item>
              <p className="text-gray-300 text-sm">
                Don't have an account? &nbsp;
                <a href="/signup" className="text-red-500 text-base hover:underline">
                  Join us!
                </a>
              </p>
            </Form.Item>

            <Form.Item className="flex justify-center items-center mb-10">
              <Button
                className="btn-user border-transparent bg-green-500 text-white hover:bg-green-400 hover:scale-105 flex justify-center items-center text-lg font-semibold tracking-wider rounded-md px-4 py-1"
                type="primary"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
        <div className="flex-1 hidden md:flex lg:flex xl:flex 2xl:flex h-full justify-center items-center"></div>
      </div>
    </div>
  );
}
