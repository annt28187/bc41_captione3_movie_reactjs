import React from 'react';
import Wnc from '../../assets/images/websiteunderconstruction.svg';
import { NavLink } from 'react-router-dom';

export default function ErrorPage() {
  return (
    <div className="not-found">
      <img
        src="https://www.pngitem.com/pimgs/m/561-5616833_image-not-found-png-not-found-404-png.png"
        alt="not-found"
      />
      <NavLink to="/" className="link-home">
        Go Home
      </NavLink>
    </div>
  );
}
