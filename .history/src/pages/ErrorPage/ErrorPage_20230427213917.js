import React from 'react';
import Lottie from 'lottie-react';
import Wnc from '../../assets/animates/under-construction.json';
import { NavLink } from 'react-router-dom';

export default function ErrorPage() {
  return (
    <div className="not-found w-full h-full">
      <Lottie className="lg:w-full lg:h-screen" animationData={Wnc} loop={true} />
      <NavLink to="/" className="link-home">
        <button>Về Trang Chủ</button>
      </NavLink>
    </div>
  );
}
