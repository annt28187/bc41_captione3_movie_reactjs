import React from 'react';
import Lottie from 'lottie-react';
import Wnc from '../../assets/animates/under-construction.json';
import { NavLink } from 'react-router-dom';

export default function ErrorPage() {
  return (
    <div className="not-found w-full h-full flex flex-col items-center">
      <Lottie className="lg:w-full lg:h-1/3" animationData={Wnc} loop={true} />
      <NavLink to="/" className="link-home">
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
          Về Trang Chủ
        </button>
      </NavLink>
    </div>
  );
}
