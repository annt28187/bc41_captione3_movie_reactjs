import React from 'react';
import Lottie from 'lottie-react';
import Wnc from '../../assets/animates/under-construction.json';
import { NavLink } from 'react-router-dom';

export default function ErrorPage() {
  return (
    <div className="not-found w-full h-screen">
      <Lottie animationData={Wnc} loop={true} />
      <NavLink to="/" className="link-home">
        Go Home
      </NavLink>
    </div>
  );
}
