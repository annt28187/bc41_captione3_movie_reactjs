import React from 'react';
import Wnc from '../../assets/animates/under-construction.json';
import { NavLink } from 'react-router-dom';

export default function ErrorPage() {
  return (
    <div className="not-found">
      <img src={Wnc} alt="not-found" />
      <NavLink to="/" className="link-home">
        Go Home
      </NavLink>
    </div>
  );
}
