import React from 'react';
import { useMediaQuery } from 'react-responsive';
import ListMovie from '../../components/ListMovie/ListMovie';
import TabMovie from './../../../.history/src/components/TabMovie/TabMovie_20230424204419';
import CarouselHome from '../../components/CarouselHome/CarouselHome';
import SearchMovieTabletMobile from '../../components/SearchMovie/SearchMovieTabletMobile';
import SearchMovieDesktop from '../../components/SearchMovie/SearchMovieDesktop';
import News from '../../components/News/News';

export default function HomePage() {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 1023 });
  return (
    <div>
      {isMobile ? <></> : <CarouselHome />}
      {isMobile || isTablet ? <SearchMovieTabletMobile /> : <SearchMovieDesktop />}
      <ListMovie />
      {!isMobile && !isTablet ? <TabMovie /> : <></>}
      <News />
    </div>
  );
}
