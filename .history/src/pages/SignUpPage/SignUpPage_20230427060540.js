import React from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { useDispatch } from 'react-redux';
import { NavLink, useNavigate } from 'react-router-dom';
import { userServ } from './../../services/userService';
import { AccountCircle } from '@mui/icons-material';

export default function SignUpPage() {
  let navigate = useNavigate();
  const onFinish = (values) => {
    userServ
      .postSignup(values)
      .then((res) => {
        message.success('Đăng ký thành công');
        setTimeout(() => {
          navigate('/login');
        }, 2000);
      })
      .catch((err) => {
        message.error('Đăng ký thất bại');
      });
  };

  const onFinishFailed = (errorInfo) => {};

  const [form] = Form.useForm();
  return (
    <div className="userLayout text-2xl bg-white rounded-xl p-5 sm:w-2/3 md:w-2/5 lg:w-1/3">
      <div className="userTitle text-center w-full space-y-5 mb-10">
        <AccountCircle fontSize="large" style={{ color: '#f97316' }} />
        <h2 className="text-2xl font-bold text-red-700 ">Đăng nhập</h2>
      </div>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 24,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical"
        className="space-y-8"
      >
        <Form.Item
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tài khoản!',
              whitespace: true,
            },
          ]}
        >
          <Input
            className="py-3 text-gray-800 hover:border-orange-600 focus:border-orange-600"
            placeholder="Tài Khoản *"
          />
        </Form.Item>

        <Form.Item
          name="matKhau"
          rules={[
            {
              required: true,
              type: 'regexp',
              pattern: new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/),
              message: 'Vui lòng nhập mật khẩu!',
            },
          ]}
        >
          <Input.Password
            className="py-3 text-gray-800 hover:border-orange-600 focus:border-orange-600"
            placeholder="Mật Khẩu *"
          />
        </Form.Item>

        <Form.Item
          name="confirm"
          dependencies={['matKhau']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập lại mật khẩu!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('matKhau') === value) {
                  return Promise.resolve();
                }

                return Promise.reject(new Error('Mật khẩu không trùng khớp!'));
              },
            }),
          ]}
        >
          <Input.Password
            className="py-3 text-gray-800 hover:border-orange-600 focus:border-orange-600"
            placeholder="Nhập lại mật Khẩu *"
          />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Checkbox>Nhớ tài khoản</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Button
            type="primary"
            htmlType="submit"
            className="userButton bg-orange-600 text-white mb-3 py-6 block w-full"
          >
            ĐĂNG KÝ
          </Button>
          <NavLink
            to={'/sign-in'}
            className="text-right block hover:underline underline-offset-2 decoration-black hover:decoration-black"
          >
            <span className="text-black font-medium">Bạn đã có tài khoản? Đăng nhập</span>
          </NavLink>
        </Form.Item>
      </Form>
    </div>
  );
}
