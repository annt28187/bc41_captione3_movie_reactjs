import { SET_LOADING_OFF, SET_LOADING_ON } from '../constants/loadingConstant';

const initialState = {
  isLoading: false,
};

let loadingReducer = (state = initialState, { type }) => {
  switch (type) {
    case SET_LOADING_ON: {
      state.isLoading = true;
      return { ...state };
    }
    case SET_LOADING_OFF: {
      state.isLoading = false;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
export default loadingReducer;
