import UserLayout from '../layouts/UserLayout';
import HomePage from '../pages/HomePage/HomePage';

export const userRoutes = [
  {
    url: '/',
    component: <UserLayout Component={HomePage} />,
  },
];
