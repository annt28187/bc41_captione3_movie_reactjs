import MovieLayout from '../layouts/MovieLayout';
import UserLayout from '../layouts/UserLayout';
import DetailPage from '../pages/DetailPage/DetailPage';
import ErrorPage from '../pages/ErrorPage/ErrorPage';
import HomePage from '../pages/HomePage/HomePage';
import SignInPage from '../pages/SignInPage/SignInPage';
import SignUpPage from '../pages/SignUpPage/SignUpPage';

export const userRoutes = [
  {
    url: '/',
    component: <MovieLayout Component={HomePage} />,
  },
  {
    url: '/sign-in',
    component: <UserLayout Component={SignInPage} />,
  },
  {
    url: '/sign-up',
    component: <UserLayout Component={SignUpPage} />,
  },
  {
    url: '/detail/:id',
    component: <MovieLayout Component={DetailPage} />,
  },
  {
    url: '*',
    component: <ErrorPage />,
  },
];
