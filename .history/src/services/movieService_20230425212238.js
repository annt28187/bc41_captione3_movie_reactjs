import { https } from './config';

export const movieServ = {
  getMovieList: () => {
    return https.get('/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09');
  },
  getMoiveTheater: () => {
    return https.get('/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP09');
  },
  getBannersForCarousel: () => {
    return https.get('/api/QuanLyPhim/LayDanhSachBanner');
  },
  getListTheater: () => {
    return https.get('/api/QuanLyRap/LayThongTinHeThongRap');
  },
  getDetailMoive: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
  getMovieCalendar: (id) => {
    return https.get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${id}`);
  },
  getMobileSearchMovie: (name) => {
    return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09&tenPhim=${name}`);
  },
  getDesktopSearchMovie: (id) => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
  },
  getDetailedMovie: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
  getTheaterDetailedMovie: (id) => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
  },
};
