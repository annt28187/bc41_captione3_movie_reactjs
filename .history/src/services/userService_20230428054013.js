import axios from 'axios';
import { BASE_URL, configHeaders } from './config';

export const userServ = {
  postSignin: (loginData) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: 'POST',
      data: loginData,
      headers: configHeaders(),
    });
  },
  postSignup: (loginData) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      method: 'POST',
      data: loginData,
      headers: configHeaders(),
    });
  },
};
