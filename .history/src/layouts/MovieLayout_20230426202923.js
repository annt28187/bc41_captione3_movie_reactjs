import React from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default function MovieLayout({ Component }) {
  return (
    <div className="h-full min-h-screen flex flex-col">
      <Header />
      <div>
        <Component />
      </div>
      <Footer />
    </div>
  );
}
