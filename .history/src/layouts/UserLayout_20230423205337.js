import React from 'react';
import Header from '../components/Header/Header';

export default function UserLayout({ Component }) {
  return (
    <div className="h-full min-h-screen flex flex-col">
      <Header></Header>
      <div>
        <Component />
      </div>
    </div>
  );
}
