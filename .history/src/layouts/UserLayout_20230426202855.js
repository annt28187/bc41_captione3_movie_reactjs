import React from 'react';
import Header from '../components/Header/Header';

export default function UserLayout({ Component }) {
  return (
    <div className="h-full min-h-screen flex flex-col">
      <Header />
      <div className="flex justify-center items-center h-full w-full py-40">
        <Component />
      </div>
    </div>
  );
}
