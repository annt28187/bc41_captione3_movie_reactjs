/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {},
    colors: {
      red: '#FB4226',
      gray: '#9e9e9e',
      black: '#000',
    },
  },
  plugins: [],
};
