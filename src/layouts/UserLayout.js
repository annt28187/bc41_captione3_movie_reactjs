import React from 'react';
import Header from '../components/Header/Header';

export default function UserLayout({ Component }) {
  return (
    <div className="user__layout min-h-screen">
      <Header />
      <div className="flex justify-center items-center h-full w-full py-40">
        <Component />
      </div>
    </div>
  );
}
