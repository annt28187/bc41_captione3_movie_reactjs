import { SHOW_TRAILER } from '../constants/trailerConstant';

const initialState = {
  status: 'hidden',
  url: '',
};
let trailerReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case SHOW_TRAILER: {
      let cloneVideo = { ...state };
      cloneVideo.status = payload.status;
      cloneVideo.url = payload.url;
      return { ...state, status: cloneVideo.status, url: cloneVideo.url };
    }
    default:
      return state;
  }
};
export default trailerReducer;
