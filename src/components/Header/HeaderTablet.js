import React from 'react';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import Logo from '../../assets/images/logo-moive.png';
import UserMenu from './UserMenu';
import { Menu } from '@mui/icons-material';

export default function HeaderTablet() {
  let [menu, setMenu] = useState(-200);
  return (
    <div>
      <header className="flex justify-between px-2 items-center shadow-lg bg-white bg-opacity-90 ">
        <NavLink to="/" className="z-20">
          <img src={Logo} className="w-20" href="#header" alt="logo" />
        </NavLink>
        <button
          className="z-20 text-[#e5383b]"
          onClick={() => {
            if (menu === -200) {
              setMenu(0);
              document.querySelector('body').style = 'overflow: hidden';
            } else {
              setMenu(-200);
              document.querySelector('body').removeAttribute('style');
            }
          }}
        >
          <Menu />
        </button>
      </header>
      {/* Menu Bar*/}
      <div
        onClick={() => {
          if (menu === -200) {
            setMenu(0);
            document.querySelector('body').style = 'overflow: hidden';
          } else {
            setMenu(-200);
            document.querySelector('body').removeAttribute('style');
          }
        }}
        className="fixed h-full w-screen flex top-0 z-20"
        style={{ left: `${menu}%`, transition: '.5s' }}
      >
        <div className="bg-[#f5f3f4] h-screen w-2/4 space-y-2 py-4 px-4">
          <UserMenu />
          <hr />
          <ul className="flex flex-col space-y-2">
            <li className="mr-6">
              <a className="text-[black] font-bold hover:text-[red] duration-200" href="#listMovie">
                Lịch chiếu
              </a>
            </li>
            <li className="mr-6">
              <a className="text-[black] font-bold hover:text-[red] duration-200" href="#tabMovie">
                Cụm rạp
              </a>
            </li>
            <li className="mr-6">
              <a className="text-[black] font-bold hover:text-[red] duration-200" href="#news">
                Tin tức
              </a>
            </li>
            <li className="mr-6">
              <a className="text-[black] font-bold hover:text-[red] duration-200" href="#app">
                Ứng dụng
              </a>
            </li>
          </ul>
        </div>
        <div className="bg-[#353535] h-screen w-2/4 opacity-20">1</div>
      </div>
    </div>
  );
}
